import numpy as np
import pdb
import scipy.optimize as op
from scipy.integrate import cumtrapz
import glob
from matplotlib import pyplot as plt
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all') # tidy up any unshown plots

def lnlike(theta, x, y):
  alpha, beta, gamma, z0 = theta
  model = smail_distribution(x, alpha, beta, gamma, z0)
  return -0.5*(np.sum((y-model)**2))

def smail_distribution(z, alpha, beta, gamma, zm):
  z0 = zm/alpha
  return (z**beta) * np.exp(-(z/z0)**gamma)

population_models = glob.glob('./data/L6A5M5N2*.txt')

alpha_start = np.sqrt(2.)
beta_start = 2.
gamma_start = 1.25

z_grid = np.linspace(0,10,128)

for ip,popn in enumerate(population_models):

  z, Nz = np.loadtxt(popn, unpack=True)

  nz = Nz/cumtrapz(Nz, z)[-1]

  zm_start = cumtrapz(nz*z, z)[-1]

  nll = lambda *args: -lnlike(*args)
  result = op.minimize(nll, [alpha_start, beta_start, gamma_start, zm_start], args=(z, nz))
  alpha_ml, beta_ml, gamma_ml, z0_ml = result["x"]

  plt.figure(1, figsize=(4.5, 3.75))
  plt.clf()
  plt.plot(z, nz, 'o', label='Data')
  plt.plot(z, smail_distribution(z, alpha_ml, beta_ml, gamma_ml, z0_ml), '-', label='Fit')
  plt.legend()
  plt.title(popn.split('/')[-1].rstrip('.txt')+'\n$\\lbrace \\alpha , \\beta , \\gamma , z_0 \\rbrace = \\lbrace {0:.2f}, {1:.2f}, {2:.2f}, {3:.2f} \\rbrace$'.format(alpha_ml, beta_ml, gamma_ml, z0_ml))
  plt.xlabel('$z$')
  plt.ylabel('$n(z)$')
  plt.savefig('plots/{0}.png'.format(popn.split('/')[-1].rstrip('.txt')), dpi=300, bbox_inches='tight')

  plt.figure(2, figsize=(4.5, 3.75))
  line = plt.plot(z_grid, Nz.sum()*smail_distribution(z_grid, alpha_ml, beta_ml, gamma_ml, z0_ml), '-', label=popn.split('/')[-1].rstrip('.txt'))
  plt.plot(z, Nz, 'o', color=line[0].get_color())

plt.legend()
plt.xlabel('$z$')
plt.ylabel('$N(z)$')
plt.savefig('plots/nz_fits.png', dpi=300, bbox_inches='tight')