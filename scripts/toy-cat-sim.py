import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc
from scipy import interpolate

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all') # tidy up any unshown plots

z_arr = np.loadtxt('cosmosis-output/distances/z.txt')[::-1]
dl_arr = np.loadtxt('cosmosis-output/distances/d_l.txt')[::-1]
dl_spl = interpolate.UnivariateSpline(z_arr, dl_arr)

n_obs = 20 # see Tamanini et al e.g. Figure 3.

z_true = np.random.choice(z_arr, size=20)

sigma_z = 0.005*(1 + z_true)

z_obs = z_true + np.random.normal(loc=0.0, scale=sigma_z)

dl_true = dl_spl(z_true)

sigma_d_gw = 0.05*(1. + dl_true)

dl_obs = dl_true + np.random.normal(loc=0.0, scale=sigma_d_gw)

v_rms = 500.*np.ones_like(dl_obs)

np.savetxt('./data/toy_data.txt', np.column_stack([dl_obs, z_obs, sigma_d_gw, sigma_z, v_rms]), header='# d_GW   z_obs   sigma_d_GW   sigma_z   v_rms')

plt.figure(1, figsize=(4.5, 3.75))
plt.plot(z_arr, dl_arr*1.e-3, 'k-', alpha=0.4)
plt.errorbar(z_obs, dl_obs*1.e-3, xerr=sigma_z, yerr=sigma_d_gw*1.e-3, ms=3.0, fmt='o', color='powderblue', zorder=10)
plt.xlabel('Redshift $z$')
plt.ylabel('Luminosity Distance $d_L\,[\mathrm{Gpc}]$')
plt.xlim([z_arr.min(),z_arr.max()])
plt.ylim([dl_arr.min()*1.e-3, dl_arr.max()*1.e-3])
plt.savefig('./plots/toy_catalogue.png', dpi=300, bbox_inches='tight')