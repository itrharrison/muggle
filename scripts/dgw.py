import sys,os,pdb
import numpy as np
from scipy import interpolate, integrate
from cosmosis.datablock import names, option_section

likes = names.likelihoods
options = option_section
cosmo = names.cosmological_parameters
distances = names.distances

run_dir=os.path.split(__file__)[0] #detect path of this directory

alpha_m_modes = ['propto_omega', 'static']

def setup(block):
    datadir = block.get_string(options,"dirname", default=run_dir)
    datafile = block.get_string(options, "filename")
    alpha_m_mode = block.get_string(options, 'alpha_m_mode')

    if alpha_m_mode not in alpha_m_modes:
        raise RuntimeError('dGW alpha_m mode \'{0}\' not recognised\nPlease choose an alpha_m_mode from {1}'.format(alpha_m_mode, alpha_m_modes))

    datapath=os.path.join(datadir,datafile)

    d_gw_obs, z_obs, sigma_dgw, sigma_z, v_rms = np.loadtxt(datapath, unpack=True, comments='#')

    return d_gw_obs, z_obs, sigma_dgw, sigma_z, v_rms, alpha_m_mode

def Dfunc(z, om, ok, olam, alpha_m):
    
    Hrat = np.sqrt(om*(1. + z)**3. + ok*(1. + z)**2. + olam)
    ol_evol = olam/Hrat**2
    integrand = 0.5*alpha_m*ol_evol/(1+z)
    
    return integrand

def likelihood(data_vec, z_model_table, dl_model_table, block):

    H0 = block[cosmo, 'h0']*100
    om = block[cosmo, 'omega_m']
    ok = block[cosmo, 'omega_k']
    olam = block[cosmo, 'omega_lambda_smg']
    #alpha_m = block['horndeski_parameters', 'alpha_m']
    alpha_m = block['horndeski_parameters', 'parameters_smg__3']
    
    d_gw_obs, z_obs, sigma_dgw, sigma_z, v_rms, alpha_m_mode = data_vec

    # ToDo: check if the model being evaluated is stable (e.g. M^2 > 0)
    # return -inf if not.

    # get cosomological parameters for H(z)
    Hz = H0*np.sqrt(om*(1. + z_obs)**3. + ok*(1. + z_obs)**2. + olam)

    dl_spl = interpolate.UnivariateSpline(z_model_table, dl_model_table)
    dl_dspl = dl_spl.derivative()
    d_theory = dl_spl(z_obs)

    if alpha_m_mode=='propto_omega':
        bigD=np.zeros(len(z_obs))    
        for j in np.arange(len(z_obs)):
            bigD[j] = integrate.quad(Dfunc,0.,z_obs[j], args=(om, ok, olam, alpha_m,))[0]

        d_gw_theory = np.exp(1.*bigD)*d_theory
    elif alpha_m_mode=='static':
        d_gw_theory = (1. + z_obs)**(alpha_m/2.)*d_theory

    dd_gwdz = dl_dspl(z_obs)

    sigma_n = np.sqrt(sigma_dgw**2. + (dd_gwdz*sigma_z)**2.)
    sigma_lens = d_gw_obs*0.066*((1. - (1 + z_obs)**-0.25)/0.25)**1.8 # Tamanini et al eq.7.3
    sigma_v = d_gw_obs*(1 + 3.e5*(1. + z_obs)/(Hz*d_gw_obs))*(v_rms/3.e5)
    
    chisquare = (1./(sigma_n**2. + sigma_lens**2. + sigma_v**2.))*(d_gw_obs - d_gw_theory)**2.
    LogLike = -0.5*chisquare.sum()
    return LogLike
  
def execute(block,config):

    #Get dl(z) for theory model
    z_model_table = block[distances, 'z']
    dl_model_table = block[distances, 'd_l']

    #Make sure z is increasing
    if z_model_table[1]<z_model_table[0]:
        z_model_table = z_model_table[::-1]
        dl_model_table = dl_model_table[::-1]

    #calculate the log-likelihood
    #LogLike=likelihood(config,z_model_table, dl_model_table, H0, om, ok, olam, alpha_m)
    LogLike=likelihood(config,z_model_table, dl_model_table, block)

    #Give a little warning about infinity and NaN errors
    if not np.isfinite(LogLike):
        sys.stderr.write("Non-finite LogLike in d_gw_like\n")

    #Save the result
    block[likes, 'DGW_LIKE'] = float(LogLike)

    #Signal success
    return 0