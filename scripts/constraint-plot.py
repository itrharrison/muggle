import numpy as np
from matplotlib import pyplot as plt
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)

plt.close('all') # tidy up any unshown plots

def sigma_alpha_m(z, gw_distance_uncertainty, em_distance_uncertainty):
  retVar = 2./(np.log(1. + z))*np.sqrt(gw_distance_uncertainty**2. + em_distance_uncertainty**2.)
  return retVar

# amendola equation 11
z_arr = np.linspace(0,10,128)
sigma_amendola = sigma_alpha_m(z_arr, 0.05, 0.03)

plt.figure(1, figsize=(4.5, 3.75))
plt.fill_between(z_arr, sigma_amendola, -sigma_amendola, color='powderblue')
plt.axhline(0, linestyle='dashed', color='k')
plt.xlim([0,10])
plt.xlabel('Redshift $z$')
plt.ylabel('$\\sigma_{\\alpha_{m}}$')
plt.savefig('./plots/amendola.png', dpi=300, bbox_inches='tight')