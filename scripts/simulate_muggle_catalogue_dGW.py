import matplotlib
matplotlib.use('Agg')
import sys,os
import pdb
import numpy as np
from scipy import interpolate, integrate
from scipy import stats
from cosmosis.datablock import names, option_section

'''
- With d_GW as data
  - produce constraints with evolution

- With A+ Ax as data
  - Produce single simulated catalogues
  - Produce constraints with evolution
  - Produce catalogues from event rates and produce constraints with evolution
'''

likes = names.likelihoods
options = option_section
cosmo = names.cosmological_parameters
distances = names.distances

RSEED=12345 #ToDo: better behaviour than this

run_dir=os.path.split(__file__)[0] #detect path of this directory

error_modes = ['du-approx', 'mortlock-approx']
#catalogue_modes = ['curve', 'event_rate']
catalogue_modes = ['curve-sample', 'curve-fixed']

def Dfunc(z, om, ok, olam, alpha_m):
    
    Hrat = np.sqrt(om*(1. + z)**3. + ok*(1. + z)**2. + olam)
    ol_evol = olam/Hrat**2
    integrand = 0.5*alpha_m*ol_evol/(1+z)
    
    return integrand

def setup(block):

  catalogue_mode = block.get_string(options, 'catalogue_mode')
  error_mode = block.get_string(options, 'error_mode')
  n_events = block.get_int(options, 'nsample')
  sigma_z0 = block.get(options, 'sigma_z0')
  event_rate_fname = block.get_string(options, 'event_rate_filename')
  output_fname_base = block.get_string(options, 'output_filename_base')
  make_plots = block.get_bool(options, 'make_plots')

  if catalogue_mode not in catalogue_modes:
    raise RuntimeError('dGW catalogue mode \'{0}\' not recognised\nPlease choose a catalogue_mode from {1}'.format(catalogue_mode, catalogue_modes))

  if error_mode not in error_modes:
    raise RuntimeError('dGW error mode \'{0}\' not recognised\nPlease choose an error_mode from {1}'.format(error_mode, error_modes))

  if catalogue_mode.startswith('curve'):
    return {'nsample' : n_events,
            'event_rate_fname' : event_rate_fname,
            'sigma_z0' : sigma_z0,
            'catalogue_mode' : catalogue_mode,
            'error_mode' : error_mode,
            'event_rate_fname' : event_rate_fname,
            'output_fname_base' : output_fname_base,
            'make_plots' : make_plots}

def execute(block,config):

    np.random.seed(seed=RSEED)

    sigma_z0 = config['sigma_z0']
    catalogue_mode = config['catalogue_mode']
    error_mode = config['error_mode']
    output_fname_base = config['output_fname_base']

    H0 = block[cosmo, 'h0']*100
    om = block[cosmo, 'omega_m']
    ok = block[cosmo, 'omega_k']
    olam = block[cosmo, 'omega_lambda']
    alpha_m = block['horndeski_parameters', 'alpha_m']

    z_model_table = block[distances, 'z']
    dl_model_table = block[distances, 'd_l']

    #Make sure z is increasing
    if z_model_table[1]<z_model_table[0]:
      z_model_table = z_model_table[::-1]
      dl_model_table = dl_model_table[::-1]

    dl_spl = interpolate.UnivariateSpline(z_model_table, dl_model_table)

    if catalogue_mode.startswith('curve'):
      # read in a single event rate curve and turn into a catalogue
      # **could** sample over event properties with a fixed event number,
      # or take a Poisson error on number of detections and sample over that too...
      # i.e. nsample = np.random.poisson(scale=nsample_mean)

      nsample = config['nsample']
      if catalogue_mode=='curve-sample':
        nsample = np.random.poisson(nsample)
      event_rate_fname = config['event_rate_fname']
      
      # read in a fixed curve and sample redshifts from it for a fixed number of events
      event_z, event_count = np.loadtxt(event_rate_fname, unpack=True)
      event_dz = np.abs(event_z[1]-event_z[0])
      event_count = event_count/event_count.sum()
      
      event_z = np.concatenate([[0], event_z, [event_z[-1]+event_dz, event_z[-1]+2*event_dz]])
      #event_z = np.concatenate([[0], event_z, [8., 8.5]])
      event_count = np.concatenate([[0], event_count, [0,0]])

      z_spl = interpolate.interp1d(event_z, event_count, kind='cubic')
      
      class spline_pdf(stats.rv_continuous):
        def _pdf(self,x):
          return z_spl(x)
      
      z_pdf = spline_pdf(momtype=0, a=event_z[0], b=event_z[-1])
      z_true = z_pdf.rvs(size=nsample)

      sigma_z = sigma_z0*(1. + z_true)
      cat_z_obs = z_true + np.random.normal(loc=0.0, scale=sigma_z)
      cat_sigma_z = sigma_z0*(1. + cat_z_obs)

      realisation_stamp = 0

    if catalogue_mode=='event_rate':
      # ToDo!
      # start from event rates, realise a catalogue
      # this would require sampling over number of *events* not detections
      # then calculating an expected SNR as to whether they may be detected or not
      raise RuntimeError('\'event_rate\' mode not implemented yet')

    Hz = H0*np.sqrt(om*(1. + z_true)**3. + ok*(1. + z_true)**2. + olam)

    v_rms = 5.e2

    cat_dl_true = dl_spl(z_true)

    sigma_lens = cat_dl_true*0.066*((1. - (1 + z_true)**-0.25)/0.25)**1.8
    # might be better to just randomly sample a v and add this to dl
    sigma_v = cat_dl_true*(1 + 3.e5*(1. + z_true)/(Hz*cat_dl_true))*(v_rms/3.e5)

    if error_mode=='du-approx':
      cat_sigma_inst = cat_dl_true/4.
    elif error_mode=='mortlock-approx':
      cat_sigma_inst =  cat_dl_true**2/(4.*8.e4)  

    sigma_tot = np.sqrt(sigma_lens**2. + cat_sigma_inst**2. + sigma_v**2.)
    cat_dl_obs = cat_dl_true + np.random.normal(loc=0.0, scale=sigma_tot)

    output_fname = output_fname_base+'0_'+str(error_mode)+'.txt'.format(realisation_stamp)
    np.savetxt(output_fname, np.column_stack([cat_dl_obs, cat_z_obs, cat_sigma_inst, cat_sigma_z, np.ones_like(cat_z_obs)*v_rms]))

    if config['make_plots']:
    #if True:
      from matplotlib import pyplot as plt
      from matplotlib import rc

      rc('text', usetex=False)
      rc('font', family='serif')
      rc('font', size=14)

      fig = plt.figure(figsize=(4.5, 3.75))
      plt.plot(z_model_table, dl_model_table*1.e-3, 'k-', alpha=0.4)
      plt.errorbar(cat_z_obs, cat_dl_obs*1.e-3, xerr=cat_sigma_z, yerr=cat_sigma_inst*1.e-3, ms=3.0, mec='none', fmt='o', color='powderblue', zorder=10)
      plt.xlabel('Redshift $z$')
      plt.ylabel('Luminosity Distance $d_L\,[\mathrm{Gpc}]$')
      
      alpha_m_list = [-1.0, -0.5, 0.5, 1.0]

      for alpha_m in alpha_m_list:
        alpha_m_mode = 'evolving'
        if alpha_m_mode=='evolving':
          bigD=np.zeros(len(z_model_table))    
          for j in np.arange(len(z_model_table)):
            bigD[j] = integrate.quad(Dfunc,0.,z_model_table[j], args=(om, ok, olam, alpha_m,))[0]

          d_gw_theory = np.exp(1.*bigD)*dl_model_table
          plt.plot(z_model_table, d_gw_theory*1.e-3, '--', alpha=0.4, label='$\\alpha_m = {0:.1f}$'.format(alpha_m))

      plt.legend(loc='upper left', fontsize='small')
      plt.xlim([z_model_table.min(),z_model_table.max()])
      plt.ylim([dl_model_table.min()*1.e-3, dl_model_table.max()*1.e-3])
      plt.savefig(output_fname.replace('.txt', '.png'), dpi=300, bbox_inches='tight')
      print 'Output figure:', output_fname.replace('.txt', '.png')

    return 0


def cleanup(config):
  # nothing to do here!  We just include this
  # for completeness
  return 0
'''
  def __init__(self):

  def get_z_max_gw(self):
    
    retVar = self.H_0*(G_grav/c**3.)*(self.M_bar + 3.*self.sigma_M)/(self.sigma_A*(self.rho_star - 3.))

    return retVar

  def get_mean_number_mergers(self):

    z_max = self.get_z_max_gw()
    E_z = np.sqrt(om*(1. + z_arr)**3. + ok*(1. + z_arr)**2. + olam)

    integrand = ((c/self.H_0)*da_arr**2./E_z)*(1.+z_arr)**2.
  '''
