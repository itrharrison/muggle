from cosmosis.postprocessing.plots import Tweaks
import pylab

import pickle
from IPython import embed
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import rc

rc('text', usetex=False)
rc('font', family='serif')
rc('font', size=11)

class ModifyContours(Tweaks):
    #This could also be a list of files.  Just put the base part in here,
    #not the directory, prefix, or suffix.
    filename="2D_horndeski_parameters--alpha_m_cosmological_parameters--h0"

    def run(self):
        fig = plt.gcf()
        fig.set_size_inches(1*4.5, 1*3.75)
        fig.set_tight_layout(True)
        fig.set_dpi(300)
        ax = plt.gca()
        plt.axvline(0.0, linestyle='dashed', alpha=0.4, color='k')
        plt.axhline(0.7, linestyle='dashed', alpha=0.4, color='k')
        #if you want to try this interactively you can add this:
        # from IPython import embed; embed() #press ctrl-D when finished playing
        #you don't unfortunately see the results until it finishes, I think, though you could
        # try adding a pylab.show?

        #each set has two contours in it, inner and outer 
        contour_set_1 = ax.collections[:2]
        contour_set_2 = ax.collections[2:4]

        fcol1 = mpl.artist.getp(contour_set_1[0], 'facecolor')[0]
        fcol2 = mpl.artist.getp(contour_set_2[0], 'facecolor')[0]

        p1 = mpl.patches.Patch(color=fcol1, label='Fiducial')
        #p2 = mpl.patches.Patch(color=fcol2, label='$+$JLA$+$Reiss11')
        p2 = mpl.patches.Patch(color=fcol2, label='Evolving $\\alpha_{m}$')


        ax.legend(handles=[p1, p2])
        
        #fcol = mpl.artist.getp(contour_set_1[0], 'facecolor')[0]
        #p3 = mpl.patches.Patch(edgecolor='k', facecolor='none', label='$\mathrm{SKA1} \\times \, \mathrm{LSST}$')
        
        ax.set_xlim([-0.5,0.5])
        ax.set_ylim([0.60,0.80])
        ax.set_xticks([-0.3,0.0,0.3])
        ax.set_yticks([0.65,0.70,0.75])
        ax.set_xticklabels(['-0.3','0.0','0.3'])
        #ax.tick_params(axis='x', pad=10, labelsize=32)
        #ax.tick_params(axis='y', pad=10, labelsize=32)
        ax.set_yticklabels(['65','70','75'])
        #ax.set_ylabel('')
        #ax.set_xlabel('')
        #pickle.dump(ax, file('matter-axes-pyplot.p', 'w'))
        #fig.set_size_inches(6.5, 4.75)
        #fig.set_tight_layout(True)
        ax.set_xlabel('$\\alpha_{m}$')
        ax.set_ylabel('$H_0 \, [\mathrm{km \, s}^{-1} \, \mathrm{Mpc}^{-1}]$')
        #fig.set_dpi(450)
        #you could do the same for contour set 2, etc.,  here.
        #just remember that 2 will always be drawn on top of 1; you may
        #need to choose the order of chain files on the command line accordingly
